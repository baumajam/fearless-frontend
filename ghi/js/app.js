function createCard(title, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col-4">
      <div class="card" style="box-shadow: 7px 7px 3px; margin: 18px;">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-body-secondary">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-body-secondary">
            ${startDate}-${endDate}
        </div>
      </div>
    </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const location = details.conference.location.name;
                let startDate = new Date (details.conference.starts);
                startDate = startDate.toLocaleDateString();
                let endDate = new Date (details.conference.ends);
                endDate = endDate.toLocaleDateString();

                const html = createCard(title, description, pictureUrl, startDate, endDate, location);
                const column = document.querySelector('.row');
                column.innerHTML += html;
            }
        }

      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }

  });
